# Wavefront 3D Model Loader

This is a simple, and incomplete [Wavefront `.obj` Model File](https://en.wikipedia.org/wiki/Wavefront_.obj_file) loader.
It currently only loads the `.obj` file contents, not the `.mtl` contents, if any.

## Usage

```go
package demo

include wfm "gitlab.com/corruptmemory/wavefrontmodel"

func doTheVertexThing() {
	model, err := wfm.LoadWavefrontFile("cube.obj")
	if err == nil {
		// Iterate over the objects in the model
		for _, o := range model.Objects {
			// Iterate over the faces of the object
			for _, f := range o.Faces {
				// Iterate over the vertex indices 
				for _, v := range f.Vertices {
					// Pull out a particular vertex
					vertex := o.Vertices[v]
					// Do something with the vertex
				}
			}
		}
	}	
}
```
