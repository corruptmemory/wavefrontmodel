// Copyright (c) 2021. Jim Powers <jim@corruptmemory.com>.  All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package wavefrontmodel

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	lin "github.com/xlab/linmath"
)

type FaceFlag uint32

const (
	SmoothShading FaceFlag = 0x0001
)

func (ff FaceFlag) String() string {
	var flags []string

	if ff&SmoothShading != 0 {
		flags = append(flags, "SmoothShading")
	}

	return strings.Join(flags, " | ")
}

type FaceIndices struct {
	Vertices        []uint32
	TextureVertices []uint32
	Normals         []uint32
	Flags           FaceFlag
}

type Object struct {
	Vertices      []lin.Vec4
	TextureCoords []lin.Vec3
	VertexNormals []lin.Vec3
	Faces         []FaceIndices
	Lines         [][]uint32
	UseMtl        string
	Object        string
}

type scanner struct {
	eos               uint32
	bytes             []byte
	pos               uint32
	line              uint32
	vertexBase        uint32
	textureCoordsBase uint32
	vertexNormalsBase uint32
}

func (s *scanner) advancePos(amount uint32) bool {
	if !s.finished() {
		s.pos += amount
		s.eosFixup()
		return !s.finished()
	}
	return false
}

func (s *scanner) advanceLine(amount uint32) {
	s.line += amount
}

func (s *scanner) setVertexBaseIndex(point uint32) {
	s.vertexBase = point
}

func (s *scanner) setTextureCoordsBaseIndex(point uint32) {
	s.textureCoordsBase = point
}

func (s *scanner) setVertexNormalsBaseIndex(point uint32) {
	s.vertexNormalsBase = point
}

func (s *scanner) byteAtPos() byte {
	return s.bytes[s.pos]
}

func newWavefrontObjectScanner(bytes []byte) scanner {
	return scanner{
		eos:               uint32(len(bytes)),
		bytes:             bytes,
		pos:               0,
		line:              0,
		vertexBase:        1,
		textureCoordsBase: 1,
		vertexNormalsBase: 1,
	}
}

func (s *scanner) finished() bool {
	return s.pos == s.eos
}

func (s *scanner) eosFixup() {
	if s.pos > s.eos {
		s.pos = s.eos
	}
}

func (s *scanner) pastEol() bool {
	for !s.finished() {
		v := s.byteAtPos()
		switch v {
		case '\r':
			if !s.advancePos(1) {
				return false
			}
			fallthrough
		case '\n':
			if !s.advancePos(1) {
				return false
			}
			s.advanceLine(1)
			return true
		default: // nothing
			if !s.advancePos(1) {
				return false
			}
			continue
		}
	}
	return false
}

/* consume_to_eol will return the bytes from the current position to the end of line
 * _excluding_ comments.  Which are completely ignored.
 */
func (s *scanner) consumeToEol() []byte {
	start, end := s.pos, s.pos
	for !s.finished() {
		v := s.byteAtPos()
		switch v {
		case '\r':
			end = s.pos
			if !s.advancePos(1) {
				return s.bytes[start:end]
			}
			fallthrough
		case '\n':
			// This is here because of the fallthrough case.  This is triggered
			// only for Unix-style line endings.  Otherwise we have Windows-style line
			// endings.
			if v == '\n' {
				end = s.pos
			}
			if !s.advancePos(1) {
				return s.bytes[start:end]
			}
			s.advanceLine(1)
			return s.bytes[start:end]
		case '#':
			if s.pos == start {
				for !s.finished() {
					v = s.byteAtPos()
					switch v {
					case '\r':
						if !s.advancePos(1) {
							return s.bytes[start:end]
						}
						fallthrough
					case '\n':
						if !s.advancePos(1) {
							return s.bytes[start:end]
						}
						s.advanceLine(1)
						return s.bytes[start:end]
					}
					if !s.advancePos(1) {
						return s.bytes[start:end]
					}
				}
				return s.bytes[s.eos-1 : s.eos]
			} else {
				// Since we have started a comment, we will need to record as the "end"
				// the last word character.  We will then do our best to move to the end of line
				for i := s.pos; i > 0; i -= 1 {
					v = s.bytes[i]
					if v != ' ' && v != '\t' {
						end = i
						break
					}
				}
				for !s.finished() {
					v = s.byteAtPos()
					switch v {
					case '\r':
						if !s.advancePos(1) {
							return s.bytes[start:end]
						}
						fallthrough
					case '\n':
						if !s.advancePos(1) {
							return s.bytes[start:end]
						}
						s.advanceLine(1)
						return s.bytes[start:end]
					}
					if !s.advancePos(1) {
						return s.bytes[start:end]
					}
				}
				return s.bytes[start:s.eos]
			}
		default:
			if !s.advancePos(1) {
				return s.bytes[start:end]
			}
			continue
		}
	}
	return s.bytes[start:s.eos]
}

func (s *scanner) consumeNumber() []byte {
	start, end := s.pos, s.pos
stop:
	for !s.finished() {
		switch s.byteAtPos() {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', '.', 'E', 'e':
			end = s.pos
			if !s.advancePos(1) {
				return s.bytes[start:end]
			}
			continue
		default:
			end = s.pos
			break stop
		}
	}
	s.pastWhitespace()
	return s.bytes[start:end]
}

func (s *scanner) consumeF32() (float32, error) {
	bs := s.consumeNumber()
	r64, err := strconv.ParseFloat(string(bs), 32)
	return float32(r64), err
}

func (s *scanner) consumeU32() (uint32, error) {
	bs := s.consumeNumber()
	r, err := strconv.ParseUint(string(bs), 10, 32)
	return uint32(r), err
}

func (s *scanner) pastWhitespace() {
	for !s.finished() {
		switch s.byteAtPos() {
		case ' ', '\t':
			if !s.advancePos(1) {
				return
			}
			continue
		default:
			return
		}
	}
	return
}

func (s *scanner) keywordCheck(keyword string) bool {
	here := string(s.bytes[s.pos:])
	if strings.HasPrefix(here, keyword) {
		kl := uint32(len(keyword))
		pos := kl + s.pos
		if pos < s.eos {
			switch s.bytes[pos] {
			case ' ', '\t':
				s.pos = pos
				s.pastWhitespace()
				return true
			default:
				return false
			}
		}
	}
	return false
}

const (
	validStarts    = "flomsuv"
	topLevelStarts = "om"
	fKeyword       = "f"
	lKeyword       = "l"
	mtllibKeyword  = "mtllib"
	oKeyword       = "o"
	sKeyword       = "s"
	usemtlKeyword  = "usemtl"
	vKeyword       = "v"
	vnKeyword      = "vn"
	vtKeyword      = "vt"
	// bevelKeyword     = "bevel"
	// bmatKeyword      = "bmat"
	// cInterpKeyword   = "c_interp"
	// callKeyword      = "call"
	// conKeyword       = "con"
	// cshKeyword       = "csh"
	// cstypeKeyword    = "cstype"
	// ctechKeyword     = "ctech"
	// curvKeyword      = "curv"
	// curv2Keyword     = "curv2"
	// dInterpKeyword   = "d_interp"
	// degKeyword       = "deg"
	// endKeyword       = "end"
	// gKeyword         = "g"
	// holeKeyword      = "hole"
	// lodKeyword       = "lod"
	// mgKeyword        = "mg"
	// pKeyword         = "p"
	// parmKeyword      = "parm"
	// scrvKeyword      = "scrv"
	// shadowObjKeyword = "shadow_obj"
	// spKeyword        = "sp"
	// stechKeyword     = "stech"
	// stepKeyword      = "step"
	// surfKeyword      = "surf"
	// traceObjKeyword  = "trace_obj"
	// trimKeyword      = "trim"
	// vpKeyword        = "vp"
)

type FaceOffsetInfo struct {
	Offset uint32
	Flags  FaceFlag
}

func (s *scanner) parse() (Object, error) {
	object := Object{}
	var vertices []uint32
	var textureCoords []uint32
	var vertexNormals []uint32
	var faces []FaceOffsetInfo
	var lines []uint32
	var currentFaceFlags FaceFlag

	// We start out at the beginning of a line.  the value we find there determines what we do next
useFound:
	for !s.finished() {
		v := s.byteAtPos()
		switch {
		case strings.IndexByte(validStarts, v) > -1:
			switch v {
			case 'm':
			case 'o':
				if s.keywordCheck(oKeyword) {
					break useFound
				} else {
					s.pastEol()
				}
			case 'u':
				if s.keywordCheck(usemtlKeyword) {
					u := s.consumeToEol()
					if len(u) > 0 {
						object.UseMtl = string(u)
					}
				} else {
					s.pastEol()
				}
			case 's':
				if s.keywordCheck(sKeyword) {
					s := s.consumeToEol()
					if len(s) > 0 {
						switch string(s) {
						case "on", "ON", "1", "t", "T", "true", "TRUE", "True":
							currentFaceFlags |= SmoothShading
						default: // We could check for an error.  Being lazy at the moment.
							currentFaceFlags &= ^SmoothShading
						}
					}
				} else {
					s.pastEol()
				}
			case 'v':
				switch {
				case s.keywordCheck(vKeyword):
					vertices = append(vertices, s.pos)
					s.pastEol()
				case s.keywordCheck(vtKeyword):
					textureCoords = append(textureCoords, s.pos)
					s.pastEol()
				case s.keywordCheck(vnKeyword):
					vertexNormals = append(vertexNormals, s.pos)
					s.pastEol()
				default: // Perhaps another time
					s.pastEol()
				}
			case 'f':
				if s.keywordCheck(fKeyword) {
					faces = append(faces, FaceOffsetInfo{s.pos, currentFaceFlags})
					s.pastEol()
				} else {
					s.pastEol()
				}
			case 'l':
				if s.keywordCheck(lKeyword) {
					lines = append(lines, s.pos)
					s.pastEol()
				} else {
					s.pastEol()
				}
			}
		default:
			switch v {
			case '\r':
				s.pos += 1
				fallthrough
			case '\n':
				s.pos += 1
				s.line += 1
				s.eosFixup()
			default:
				s.pastEol()
			}
		}
	}

	object.Vertices = make([]lin.Vec4, 0, len(vertices))
	for _, p := range vertices {
		s.pos = p
		var x, y, z, w float32
		var xok, yok, zok, wok error
		x, xok = s.consumeF32()
		y, yok = s.consumeF32()
		z, zok = s.consumeF32()
		w, wok = s.consumeF32()
		if xok == nil && yok == nil && zok == nil {
			v := lin.Vec4{x, y, z, 1.0}
			if wok == nil {
				v[3] = w
			}
			object.Vertices = append(object.Vertices, v)
		}
	}
	object.TextureCoords = make([]lin.Vec3, 0, len(textureCoords))
	for _, p := range textureCoords {
		s.pos = p
		var u, v, w float32
		var uok, vok, wok error
		u, uok = s.consumeF32()
		v, vok = s.consumeF32()
		w, wok = s.consumeF32()
		if uok == nil {
			tc := lin.Vec3{u, 0.0, 0.0}
			if vok == nil {
				tc[1] = v
			}
			if wok == nil {
				tc[2] = w
			}
			object.TextureCoords = append(object.TextureCoords, tc)
		}
	}
	object.VertexNormals = make([]lin.Vec3, 0, len(vertexNormals))
	for _, p := range vertexNormals {
		s.pos = p
		var x, y, z float32
		var xok, yok, zok error
		x, xok = s.consumeF32()
		y, yok = s.consumeF32()
		z, zok = s.consumeF32()
		if xok == nil && yok == nil && zok == nil {
			vn := lin.Vec3{x, y, z}
			object.VertexNormals = append(object.VertexNormals, vn)
		}
	}
	object.Faces = make([]FaceIndices, 0, len(faces))
	for _, p := range faces {
		s.pos = p.Offset
		var fis []uint32
		var ftis []uint32
		var fnis []uint32
		for {
			fi, fiok := s.consumeU32()
			if fiok == nil {
				fis = append(fis, fi-s.vertexBase)
				if s.bytes[s.pos] == '/' {
					s.pos += 1
					fti, ftiok := s.consumeU32()
					if ftiok == nil {
						ftis = append(ftis, fti-s.textureCoordsBase)
					}
				}
				if s.bytes[s.pos] == '/' {
					s.pos += 1
					tfni, fniok := s.consumeU32()
					if fniok == nil {
						fnis = append(fnis, tfni-s.vertexNormalsBase)
					} else {
						_, _ = s.consumeU32()
					}
				}
				continue
			}
			break
		}
		fi := FaceIndices{}
		fi.Flags = p.Flags
		if len(fis) > 0 {
			fi.Vertices = fis
		}
		if len(ftis) > 0 {
			fi.TextureVertices = ftis
		}
		if len(fnis) > 0 {
			fi.Normals = fnis
		}
		object.Faces = append(object.Faces, fi)
	}
	object.Lines = make([][]uint32, 0, len(lines))
	for _, p := range lines {
		s.pos = p
		var lis []uint32
		for {
			li, liok := s.consumeU32()
			if liok == nil {
				lis = append(lis, li-s.vertexBase)
				continue
			}
			break
		}
		if len(lis) > 0 {
			object.Lines = append(object.Lines, lis)
		}
	}
	s.vertexBase += uint32(len(vertices))
	s.textureCoordsBase += uint32(len(textureCoords))
	s.vertexNormalsBase += uint32(len(vertexNormals))

	return object, nil
}

type Model struct {
	File    string
	MtlLibs []string
	Objects []Object
}

func LoadWavefrontFile(file string) (result *Model, err error) {
	bytes, err := os.ReadFile(file)
	if err != nil {
		return
	}

	object := Model{File: file}

	err = object.ModelParser(bytes)
	if err != nil {
		return
	}
	return &object, nil
}

type sStates int

const (
	sUnitialized sStates = iota
	sTrue
	sFalse
)

func (ss sStates) String() string {
	switch ss {
	case sTrue:
		return "on"
	case sFalse:
		return "off"
	default:
		return ""
	}
}

func sState(s bool) sStates {
	if s {
		return sTrue
	}
	return sFalse
}

func writeObject(o Object, w io.Writer) error {
	_, err := fmt.Fprintf(w, "o %s\n", o.Object)
	if err != nil {
		return err
	}
	for _, vs := range o.Vertices {
		_, err := fmt.Fprint(w, "v")
		if err != nil {
			return err
		}
		for _, v := range vs[0:3] {
			_, err := fmt.Fprintf(w, " %f", v)
			if err != nil {
				return err
			}
		}
		if vs[3] != 1.0 {
			_, err := fmt.Fprintf(w, " %f", vs[3])
			if err != nil {
				return err
			}
		}
		_, err = fmt.Fprintln(w, "")
		if err != nil {
			return err
		}
	}
	for _, vts := range o.TextureCoords {
		_, err := fmt.Fprint(w, "vt")
		if err != nil {
			return err
		}
		for _, vt := range vts[0:2] {
			_, err := fmt.Fprintf(w, " %f", vt)
			if err != nil {
				return err
			}
		}
		if vts[2] != 0.0 {
			_, err := fmt.Fprintf(w, " %f", vts[2])
			if err != nil {
				return err
			}
		}
		_, err = fmt.Fprintln(w, "")
		if err != nil {
			return err
		}
	}
	for _, vns := range o.VertexNormals {
		_, err := fmt.Fprint(w, "vn")
		if err != nil {
			return err
		}
		for _, vn := range vns {
			_, err := fmt.Fprintf(w, " %f", vn)
			if err != nil {
				return err
			}
		}
		_, err = fmt.Fprintln(w, "")
		if err != nil {
			return err
		}
	}
	if o.UseMtl != "" {
		_, err := fmt.Fprintf(w, "usemtl %s\n", o.UseMtl)
		if err != nil {
			return err
		}
	}
	s := sUnitialized
	for _, f := range o.Faces {
		ts := sState(f.Flags&SmoothShading != 0)
		if s != ts {
			s = ts
			_, err := fmt.Fprintf(w, "s %s\n", s)
			if err != nil {
				return err
			}
		}
		_, err := fmt.Fprint(w, "f")
		if err != nil {
			return err
		}
		for i, fi := range f.Vertices {
			_, err := fmt.Fprintf(w, " %d", fi+1)
			if err != nil {
				return err
			}
			if i < len(f.TextureVertices) {
				_, err := fmt.Fprintf(w, "/%d", f.TextureVertices[i]+1)
				if err != nil {
					return err
				}
			}
			if i < len(f.Normals) {
				_, err := fmt.Fprintf(w, "/%d", f.Normals[i]+1)
				if err != nil {
					return err
				}
			}
		}
		_, err = fmt.Fprintln(w, "")
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *Model) ToWriter(w io.Writer) error {
	for _, l := range m.MtlLibs {
		_, err := fmt.Fprintf(w, "mtllib %s\n", l)
		if err != nil {
			return err
		}
	}
	for _, o := range m.Objects {
		err := writeObject(o, w)
		if err != nil {
			return err
		}
	}
	return nil
}

func (objectFile *Model) ModelParser(bytes []byte) error {
	scanner := newWavefrontObjectScanner(bytes)

	var objectName string

	// We start out at the beginning of a line.  the value we find there determines what we do next
	for !scanner.finished() {
		v := scanner.byteAtPos()
		switch {
		case strings.IndexByte(topLevelStarts, v) > -1:
			switch v {
			case 'm':
				if scanner.keywordCheck(mtllibKeyword) {
					m := scanner.consumeToEol()
					if len(m) > 0 {
						objectFile.MtlLibs = append(objectFile.MtlLibs, string(m))
					}
				} else {
					scanner.pastEol()
				}
			case 'o':
				if scanner.keywordCheck(oKeyword) {
					o := scanner.consumeToEol()
					if len(o) > 0 {
						objectName = string(o)
						obj, err := scanner.parse()
						if err == nil {
							obj.Object = objectName
							objectFile.Objects = append(objectFile.Objects, obj)
						}
					}
					fmt.Println("Finished with object")
				} else {
					scanner.pastEol()
				}
			}
		default:
			switch v {
			case '\r':
				scanner.pos += 1
				fallthrough
			case '\n':
				scanner.pos += 1
				scanner.line += 1
				scanner.eosFixup()
			default:
				scanner.pastEol()
			}
		}
	}

	return nil
}
