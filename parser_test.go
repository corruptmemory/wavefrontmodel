// Copyright (c) 2021. Jim Powers <jim@corruptmemory.com>.  All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package wavefrontmodel

import (
	"testing"
)

const cube = `# Blender v2.91.2 OBJ File: ''
# www.blender.org
mtllib cube2.mtl
o Cube
v 1.000000 1.000000 -1.000000
v 1.000000 -1.000000 -1.000000
v 1.000000 1.000000 1.000000
v 1.000000 -1.000000 1.000000
v -1.000000 1.000000 -1.000000
v -1.000000 -1.000000 -1.000000
v -1.000000 1.000000 1.000000
v -1.000000 -1.000000 1.000000
vt 0.875000 0.500000
vt 0.625000 0.750000
vt 0.625000 0.500000
vt 0.375000 1.000000
vt 0.375000 0.750000
vt 0.625000 0.000000
vt 0.375000 0.250000
vt 0.375000 0.000000
vt 0.375000 0.500000
vt 0.125000 0.750000
vt 0.125000 0.500000
vt 0.625000 0.250000
vt 0.875000 0.750000
vt 0.625000 1.000000
vn 0.0000 1.0000 0.0000
vn 0.0000 0.0000 1.0000
vn -1.0000 0.0000 0.0000
vn 0.0000 -1.0000 0.0000
vn 1.0000 0.0000 0.0000
vn 0.0000 0.0000 -1.0000
usemtl Material
s off
f 5/1/1 3/2/1 1/3/1
f 3/2/2 8/4/2 4/5/2
f 7/6/3 6/7/3 8/8/3
f 2/9/4 8/10/4 6/11/4
f 1/3/5 4/5/5 2/9/5
f 5/12/6 2/9/6 6/7/6
s on
f 5/1/1 7/13/1 3/2/1
f 3/2/2 7/14/2 8/4/2
s off
f 7/6/3 5/12/3 6/7/3
f 2/9/4 4/5/4 8/10/4
f 1/3/5 3/2/5 4/5/5
f 5/12/6 1/3/6 2/9/6`


func TestParser(t *testing.T) {
	obj := Model{}
	err := obj.ModelParser([]byte(cube))
	if err != nil {
		t.Errorf("error: failed to parse valid obj file: %v", err)
		t.FailNow()
	}
	if len(obj.MtlLibs) != 1 {
		t.Errorf("error: expected 1 MtlLib, got: %d", len(obj.MtlLibs))
		t.FailNow()
	}
	if obj.MtlLibs[0] != "cube2.mtl" {
		t.Errorf("error: expected the single MTL Library to be 'cube2.mtl', got: %s", obj.MtlLibs[0])
		t.FailNow()
	}
	if len(obj.Objects) != 1 {
		t.Errorf("error: expected 1 object, got: %d", len(obj.Objects))
		t.FailNow()
	}
	o := obj.Objects[0]
	if o.Object != "Cube" {
		t.Errorf("error: object should equal 'Cube', got: %s", o.Object)
		t.FailNow()
	}
	if len(o.Vertices) != 8 {
		t.Errorf("error: expected 8 verticies, got: %d", len(o.Vertices))
		t.FailNow()
	}
	if len(o.Lines) != 0 {
		t.Errorf("error: expected 0 lines, got: %d", len(o.Lines))
		t.FailNow()
	}
	if len(o.TextureCoords) != 14 {
		t.Errorf("error: expected 14 texture coordinates, got: %d", len(o.TextureCoords))
		t.FailNow()
	}
	if len(o.VertexNormals) != 6 {
		t.Errorf("error: expected 6 vertex normals, got: %d", len(o.VertexNormals))
		t.FailNow()
	}
	if o.UseMtl != "Material" {
		t.Errorf("error: expected the UseMtl value to be 'Material', got: %s", o.UseMtl)
		t.FailNow()
	}
	if len(o.Faces) != 12 {
		t.Errorf("error: expected 12 faces, got: %d", len(o.Faces))
		t.FailNow()
	}
}
